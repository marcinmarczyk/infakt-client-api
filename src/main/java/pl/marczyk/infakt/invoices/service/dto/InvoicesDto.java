package pl.marczyk.infakt.invoices.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class InvoicesDto {

    private MetainfoDto metainfo;
    private List<InvoiceDto> entities;

}
