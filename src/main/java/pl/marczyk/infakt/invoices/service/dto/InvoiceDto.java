package pl.marczyk.infakt.invoices.service.dto;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InvoiceDto {
    Integer id;
    String number;
    String currency;
    @SerializedName("paid_price")
    Integer paidPrice;
    String notes;
    String kind;
    @SerializedName("payment_method")
    String paymentMethod;
    @SerializedName("recipient_signature")
    String recipientSignature;
    @SerializedName("seller_signature")
    String sellerSignature;
    @SerializedName("invoice_date")
    String invoiceDate;
    @SerializedName("sale_date")
    String saleDate;
    String status;
    @SerializedName("payment_date")
    String paymentDate;
    @SerializedName("net_price")
    Integer netPrice;
    @SerializedName("tax_price")
    Integer taxPrice;
    @SerializedName("gross_price")
    Integer grossPrice;
    @SerializedName("client_id")
    Integer clientId;
    @SerializedName("client_company_name")
    String clientCompanyName;
    @SerializedName("client_street")
    String clientStreet;
    @SerializedName("client_city")
    String clientCity;
    @SerializedName("client_post_code")
    String clientPostCode;
    @SerializedName("client_tax_code")
    String clientTaxCode;
    @SerializedName("clean_client_nip")
    String cleanClientNip;
    @SerializedName("client_country")
    String clientCountry;
    @SerializedName("check_duplicate_number")
    Boolean checkDuplicateNumber;
    @SerializedName("bank_name")
    String bankName;
    @SerializedName("bank_account")
    String bankAccount;
    String swift;
    @SerializedName("sale_type")
    String saleType;
    @SerializedName("invoice_date_kind")
    String invoiceDateKind;
    List<ServiceDto> services;
    @SerializedName("vat_exemption_reason")
    Integer vatExemptionReason;
    Map<String, ExtensionDto> extensions;
    @SerializedName("bdo_code")
    String bdoCode;
    @SerializedName("split_payment")
    String splitPayment;
    @SerializedName("split_payment_type")
    String splitPaymentType;
    @SerializedName("paid_date")
    String paidDate;
    @SerializedName("transaction_kind_id")
    Integer transactionKindId;
    @SerializedName("document_markings_ids")
    List<String> documentMarkingsIds;
    @SerializedName("date_kind")
    String dateKind;
}
