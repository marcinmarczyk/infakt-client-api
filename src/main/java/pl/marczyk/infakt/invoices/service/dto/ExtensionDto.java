package pl.marczyk.infakt.invoices.service.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ExtensionDto {

    Boolean available;
    String link;
    @SerializedName("valid_until")
    String validUntil;

}
