package pl.marczyk.infakt.invoices

import pl.marczyk.infakt.invoices.service.InfaktApiClientBuilder
import spock.lang.Specification

import static pl.marczyk.infakt.invoices.DtoHelper.TEST_API_KEY

class InfaktApiClientTest extends Specification {

	def "should return invoices"() {
		given:
		def expectedResult = DtoHelper.invoices()

		when:
		def infaktClient = InfaktApiClientBuilder.build()
        def invoices = infaktClient.getInvoices(TEST_API_KEY)

		then:
		invoices == expectedResult
	}

	def 'should return invoice by id'() {
		given:
		def expectedResult = DtoHelper.invoice()

		when:
		def infaktClient = InfaktApiClientBuilder.build()
		def invoice = infaktClient.getInvoice(TEST_API_KEY, DtoHelper.INVOICE_ID)

		then:
		invoice == expectedResult
	}

}
