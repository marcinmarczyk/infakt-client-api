package pl.marczyk.infakt.invoices

import pl.marczyk.infakt.invoices.service.dto.ExtensionDto
import pl.marczyk.infakt.invoices.service.dto.InvoiceDto
import pl.marczyk.infakt.invoices.service.dto.InvoicesDto
import pl.marczyk.infakt.invoices.service.dto.MetainfoDto
import pl.marczyk.infakt.invoices.service.dto.ServiceDto

class DtoHelper {

    def static final TEST_API_KEY = '5bbe0fdd87887cd0717b1a3915035cca82293ad4'

    static final Long INVOICE_ID = 34499915

    static final InvoiceDto invoice(){
        def invoice = new InvoiceDto()
        invoice.id = INVOICE_ID
        invoice.number = '1/11/2020'
        invoice.currency = 'PLN'
        invoice.paidPrice = 1230000
        invoice.notes = ''
        invoice.kind = 'vat'
        invoice.paymentMethod = 'cash'
        invoice.splitPayment = false
        invoice.splitPaymentType = null
        invoice.recipientSignature = ''
        invoice.sellerSignature = ''
        invoice.invoiceDate = '2020-11-28'
        invoice.saleDate = '2020-11-28'
        invoice.status = 'draft'
        invoice.paymentDate = '2020-12-12'
        invoice.paidDate = null
        invoice.netPrice = 1000000
        invoice.taxPrice = 230000
        invoice.grossPrice = 1230000
        invoice.clientId = 11603766
        invoice.clientCompanyName = 'kontrahent'
        invoice.clientStreet = 'Lewa'
        invoice.clientCity = 'Radom'
        invoice.clientPostCode = '51-234'
        invoice.clientTaxCode = '5118849106'
        invoice.clientCountry = 'PL'
        invoice.bankName = ''
        invoice.bankAccount = ''
        invoice.swift = ''
        invoice.vatExemptionReason = null
        invoice.saleType = ''
        invoice.invoiceDateKind = 'sale_date'
        invoice.checkDuplicateNumber = null
        invoice.cleanClientNip = null
        invoice.dateKind = null
        def service = new ServiceDto()
        invoice.services = [service]
        def extensionPayments = new ExtensionDto()
        extensionPayments.available = false
        extensionPayments.link = null
        extensionPayments.validUntil = null
        def extensionShares = new ExtensionDto()
        extensionShares.available = true
        extensionShares.link = null
        extensionShares.validUntil = null
        invoice.extensions = ['payments': extensionPayments, 'shares': extensionShares]
        invoice.bdoCode = null
        invoice.transactionKindId = null
        invoice.documentMarkingsIds = []

        service.id = 78564240
        service.name = 'Wytwarzanie oprogramowania'
        service.taxSymbol = '23'
        service.unit = ''
        service.quantity = 1
        service.unitNetPrice = 1000000
        service.netPrice = 1000000
        service.grossPrice = 1230000
        service.taxPrice = 230000
        service.symbol = ''
        service.flatRateTaxSymbol = null
        service.discount = 0
        service.unitNetPriceBeforeDiscount = 1000000
        service.gtuId = null

        invoice
    }

    static final InvoicesDto invoices() {
        def invoice = invoice()
        def invoices = new InvoicesDto()
        invoices.entities = [invoice]
        invoices.metainfo = metainfo()

        invoices
    }

    static final MetainfoDto metainfo() {
        def metainfo = new MetainfoDto()
        metainfo.next = 'https://api.infakt.pl/api/v3/invoices.json?offset=10&limit=10'
        metainfo.previous = 'https://api.infakt.pl/api/v3/invoices.json?offset=0&limit=10'
        metainfo.count = 1

        metainfo
    }
}
