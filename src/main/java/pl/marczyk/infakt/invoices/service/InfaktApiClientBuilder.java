package pl.marczyk.infakt.invoices.service;

import feign.Feign;
import feign.gson.GsonDecoder;

public abstract class InfaktApiClientBuilder {

    public static InfaktApiClient build() {
        return Feign.builder()
                .decoder(new GsonDecoder())
                .target(InfaktApiClient.class, "https://api.infakt.pl:443/api");
    }

}
