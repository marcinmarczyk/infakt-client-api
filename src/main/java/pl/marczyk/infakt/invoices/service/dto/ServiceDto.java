package pl.marczyk.infakt.invoices.service.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ServiceDto {

    Integer id;
    String name;
    @SerializedName("tax_symbol")
    String taxSymbol;
    String unit;
    Integer quantity;
    @SerializedName("unit_net_price")
    Integer unitNetPrice;
    @SerializedName("net_price")
    Integer netPrice;
    @SerializedName("gross_price")
    Integer grossPrice;
    @SerializedName("tax_price")
    Integer taxPrice;
    String symbol;
    @SerializedName("flat_rate_tax_symbol")
    String flatRateTaxSymbol;
    Integer discount;
    @SerializedName("unit_net_price_before_discount")
    Integer unitNetPriceBeforeDiscount;
    @SerializedName("gtu_id")
    Integer gtuId;

}
