package pl.marczyk.infakt.invoices.service.dto;

import lombok.Data;

@Data
public class MetainfoDto {

    private int count;
    private int totalCount;
    private String next;
    private String previous;

}
