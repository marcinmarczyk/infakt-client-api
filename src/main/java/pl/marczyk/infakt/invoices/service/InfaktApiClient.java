package pl.marczyk.infakt.invoices.service;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import pl.marczyk.infakt.invoices.service.dto.InvoiceDto;
import pl.marczyk.infakt.invoices.service.dto.InvoicesDto;

public interface InfaktApiClient {

    @RequestLine("GET /v3/invoices.json")
    @Headers("X-inFakt-ApiKey: {infaktApiKey}")
    InvoicesDto getInvoices(@Param("infaktApiKey") String apiKey);

    @RequestLine("GET /v3/invoices/{id}.json")
    @Headers("X-inFakt-ApiKey: {infaktApiKey}")
    InvoiceDto getInvoice(@Param("infaktApiKey") String apiKey, @Param("id") Long id);

}
